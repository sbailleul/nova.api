## Create docker image

```shell
./gradlew bootBuildImage --imageName=nova/nova-api
```

## Deploy on AWS 
```shell
. deploy.sh
```